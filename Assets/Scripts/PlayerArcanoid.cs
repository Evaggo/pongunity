﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerArcanoid : MonoBehaviour
{
    public float velocidadX;
    private float posX;
    private float direction;
    private Vector3 newPosition = new Vector3(0, 0, 0);
    public float maxX;


    // Update is called once per frame
    void Update()
    {

        direction = Input.GetAxis("Horizontal");

        Debug.Log(transform.position.x);
        Debug.Log(transform.rotation.x);

        posX = transform.position.x + direction * velocidadX * Time.deltaTime;

        newPosition.x = posX;
        newPosition.y = transform.position.y;
        newPosition.z = transform.position.z;
        transform.position = newPosition;

        if (posX < -maxX)
        {
            newPosition.x = -maxX;
            transform.position = newPosition;

        }

        if (posX > maxX)
        {
            newPosition.x = maxX;
            transform.position = newPosition;

        }

    }
}
