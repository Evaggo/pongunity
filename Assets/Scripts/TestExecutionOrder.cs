﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestExecutionOrder : MonoBehaviour
{
    void Awake() {
        Debug.Log("Awake");
    }

    private void OnEnable()
    {
        Debug.Log("OnEnable");
    }

    private void Start()
    {
        Debug.Log("Start");
    }

    private void Update() {
        Debug.Log("Update");
    }

    void LateUpdate() {
        Debug.LogWarning("LateUpdate");
    }
    

}
